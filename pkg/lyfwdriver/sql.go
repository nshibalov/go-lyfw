package lyfwdriver

import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/reflectx"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwquery"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

type nilDest struct{}

func (*nilDest) Scan(val interface{}) error {
	return nil
}

type SQLRet struct {
	p, v lyfwstore.Model
}

type SQLResult map[*lyfwstore.Node][]SQLRet

type SQL struct {
	db             *sqlx.DB
	queryGenerator lyfwquery.SQLGenerator
}

func (s *SQL) tx(ctx context.Context) *sqlx.Tx {
	v := ctx.Value(CtxTransaction)
	if tx, ok := v.(*sqlx.Tx); ok {
		return tx
	}
	return nil
}

func (s *SQL) ext(ctx context.Context) sqlx.Ext {
	tx := s.tx(ctx)
	if tx != nil {
		return tx
	}

	return s.db
}

func (s *SQL) idColumn(m lyfwstore.Model) string {
	return sqlx.NameMapper(m.FieldStoreName(m.IDField()))
}

func (s *SQL) columns(m lyfwstore.Model, fields ...string) []string {
	ret := make([]string, len(fields))

	for i, field := range fields {
		ret[i] = sqlx.NameMapper(m.FieldStoreName(field))
	}

	return ret
}

func (s *SQL) Begin(ctx context.Context) (context.Context, error) {
	tr, err := s.db.Begin()
	if err != nil {
		return ctx, err
	}

	return context.WithValue(ctx, CtxTransaction, tr), nil
}

func (s *SQL) Commit(ctx context.Context) error {
	tx := s.tx(ctx)
	if tx == nil {
		log.Printf("SQL driver: no transaction to commit")
		return nil
	}

	return tx.Commit()
}

func (s *SQL) Rollback(ctx context.Context) error {
	tx := s.tx(ctx)
	if tx == nil {
		log.Printf("SQL driver: no transaction to rollback")
		return nil
	}

	return tx.Rollback()
}

func (s *SQL) Create(ctx context.Context, v lyfwstore.Model) error {
	ext := s.ext(ctx)
	fields := v.FieldNames()
	columns := s.columns(v, fields...)

	values := make([]interface{}, len(fields))
	for i, field := range fields {
		v := v.FieldValue(field)
		values[i] = v
	}

	// nolint:gosec // it's ok for driver
	q := fmt.Sprintf(
		`INSERT INTO %s (%s) VALUES (%s)`,
		v.StoreName(),
		strings.Join(columns, ","),
		strings.Join(strings.Split(strings.Repeat("?", len(columns)), ""), ","))

	_, err := ext.Exec(ext.Rebind(q), values...)
	if err != nil {
		return &ErrExec{Model: v, Err: err}
	}

	return nil
}

func (s *SQL) Update(ctx context.Context, v lyfwstore.Model, fields ...string) error {
	ext := s.ext(ctx)

	if len(fields) == 0 {
		fields = make([]string, 0, len(fields))

		for _, field := range v.FieldNames() {
			if field == v.IDField() {
				continue
			}

			fields = append(fields, field)
		}
	}

	columns := make([]string, len(fields))

	values := make([]interface{}, len(fields)+1) // +1 for ID
	values[len(values)-1] = v.IDValue()

	for i, field := range fields {
		column := sqlx.NameMapper(v.FieldStoreName(field))
		columns[i] = fmt.Sprintf("%s = ?", column)
		values[i] = v.FieldValue(field)
	}

	// nolint:gosec // it's ok for driver
	q := fmt.Sprintf(
		`UPDATE %s SET %s WHERE %s = ?`,
		v.StoreName(),
		strings.Join(columns, ","),
		s.idColumn(v),
	)

	if _, err := ext.Exec(ext.Rebind(q), values...); err != nil {
		return &ErrExec{Model: v, Err: err}
	}

	return nil
}

func (s *SQL) Delete(ctx context.Context, v lyfwstore.Model) error {
	ext := s.ext(ctx)

	// nolint:gosec // it's ok for driver
	q := fmt.Sprintf(`DELETE FROM %s WHERE %s = ?`, v.StoreName(), s.idColumn(v))

	if _, err := ext.Exec(ext.Rebind(q), v.IDValue()); err != nil {
		return &ErrExec{Model: v, Err: err}
	}

	return nil
}

func (s *SQL) process(res SQLResult, node *lyfwstore.Node, parent lyfwstore.Model, resultChan chan<- lyfwstore.DriverResult) {
	var children []lyfwstore.Model

	for _, rv := range res[node] {
		if parent != nil {
			if rv.p != parent {
				continue
			}

			children = append(children, rv.v)
		}

		for _, subNode := range node.Nodes {
			s.process(res, subNode, rv.v, resultChan)
		}

		resultChan <- lyfwstore.DriverResult{Node: node, Value: rv.v}
	}

	if parent != nil {
		parent.SetRelation(node.RelationField, children...)
	}
}

func (s *SQL) scan(rows *sqlx.Rows, node *lyfwstore.Node, parent lyfwstore.Model, result SQLResult) error {
	columnToField := make(map[string]string)

	for _, field := range node.Model.FieldNames() {
		column := sqlx.NameMapper(node.Model.FieldStoreName(field))
		columnToField[column] = field
	}

	columns, _ := rows.Columns()
	dest := make([]interface{}, len(columns))

	target := node.Model.New()

	// fill
	for i, column := range columns {
		pair := strings.SplitN(column, ".", 2)
		alias, column := pair[0], pair[1]

		if alias == node.Alias {
			dest[i] = target.FieldValuePtr(columnToField[column])
		} else {
			dest[i] = nilDest{}
		}
	}

	// scan
	if err := rows.Scan(dest...); err != nil {
		return err
	}

	{ // add to result
		found := false

		for _, t := range result[node] {
			if t.v.IDValue() == target.IDValue() {
				target = t.v
				found = true
			}
		}

		if !found {
			result[node] = append(result[node], SQLRet{p: parent, v: target})
		}
	}

	// scan children
	for _, subNode := range node.Nodes {
		if err := s.scan(rows, subNode, target, result); err != nil {
			return err
		}
	}

	return nil
}

func (s *SQL) query(ctx context.Context, root *lyfwstore.Node, resultChan chan<- lyfwstore.DriverResult) {
	defer close(resultChan)

	g := s.queryGenerator.NewForNode(root)

	q, values, err := g.GenSelect(false)
	if err != nil {
		resultChan <- lyfwstore.NewDriverResultError(&ErrGenerate{Err: err})
		return
	}

	q, values, err = sqlx.In(q, values...)
	if err != nil {
		resultChan <- lyfwstore.NewDriverResultError(&ErrGenerate{Err: err})
		return
	}

	ext := s.ext(ctx)
	rows, err := ext.Queryx(ext.Rebind(q), values...)
	if err != nil {
		resultChan <- lyfwstore.NewDriverResultError(&ErrExec{Err: err})
		return
	}

	for rows.Next() {
		res := make(map[*lyfwstore.Node][]SQLRet)

		if err := s.scan(rows, root, nil, res); err != nil {
			resultChan <- lyfwstore.NewDriverResultError(&ErrScan{Err: err})
			return
		}

		s.process(res, root, nil, resultChan)
	}
}

func (s *SQL) Query(ctx context.Context, root *lyfwstore.Node) <-chan lyfwstore.DriverResult {
	resultChan := make(chan lyfwstore.DriverResult)

	go s.query(ctx, root, resultChan)

	return resultChan
}

func NewSQLDriver(db *sqlx.DB, queryGenerator lyfwquery.SQLGenerator) lyfwstore.Driver {
	db = sqlx.NewDb(db.DB, db.DriverName())
	db.Mapper = reflectx.NewMapperFunc(lyfwstore.TagStore, sqlx.NameMapper)

	return &SQL{db: db, queryGenerator: queryGenerator}
}

func NewPGSQLDriver(db *sqlx.DB) lyfwstore.Driver {
	return NewSQLDriver(db, lyfwquery.NewPGSQLGenerator(nil, sqlx.NameMapper))
}
