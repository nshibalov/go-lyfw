package lyfwdriver

import (
	"context"
	"log"
	"sync"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

type Dummy struct {
	trID uint64
	m    sync.Mutex
}

func (b *Dummy) newTransaction() uint64 {
	b.m.Lock()
	defer b.m.Unlock()

	b.trID++

	return b.trID
}

func (b *Dummy) Begin(ctx context.Context) (context.Context, error) {
	tr := b.newTransaction()

	log.Printf("dummy driver: begin (tr = %d)", tr)

	return context.WithValue(ctx, CtxTransaction, tr), nil
}

func (b *Dummy) Commit(ctx context.Context) error {
	var tr uint64

	if v := ctx.Value(CtxTransaction); v == nil {
		log.Printf("dummy driver: no transaction to commit")
	} else {
		tr = v.(uint64)
	}

	log.Printf("dummy driver: commit (tr = %d)", tr)

	return nil
}

func (b *Dummy) Rollback(ctx context.Context) error {
	var tr uint64

	if v := ctx.Value(CtxTransaction); v == nil {
		log.Printf("dummy driver: no transaction to rollback")
	} else {
		tr = v.(uint64)
	}

	log.Printf("dummy driver: rollback (tr = %d)", tr)

	return nil
}

func (b *Dummy) Create(ctx context.Context, v lyfwstore.Model) error {
	log.Printf("dummy driver: create %+v", v)

	return nil
}

func (b *Dummy) Update(ctx context.Context, v lyfwstore.Model, fields ...string) error {
	log.Printf("dummy driver: update %+v, fields: %v", v, fields)

	return nil
}

func (b *Dummy) Delete(ctx context.Context, v lyfwstore.Model) error {
	log.Printf("dummy driver: delete %+v", v)

	return nil
}

func (b *Dummy) Query(ctx context.Context, tree *lyfwstore.Node) <-chan lyfwstore.DriverResult {
	log.Printf("dummy driver: query %+v", tree)

	resultChan := make(chan lyfwstore.DriverResult)

	go func() {
		defer close(resultChan)
	}()

	return resultChan
}
