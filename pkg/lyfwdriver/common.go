package lyfwdriver

// CtxKey - Type for Store context values
type CtxKey uint

const (
	// CtxTransaction - This context value contains Driver's transaction object (if any)
	CtxTransaction CtxKey = iota
)
