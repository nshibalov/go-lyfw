package lyfwdriver

import (
	"fmt"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

type ErrGenerate struct {
	Err error
}

func (e *ErrGenerate) Error() string {
	return fmt.Sprintf("error generating query: %s", e.Err)
}

type ErrExec struct {
	Model lyfwstore.Model
	Err   error
}

func (e *ErrExec) Error() string {
	if e.Model != nil {
		return fmt.Sprintf("error executing query for model %s with id '%s': %s", e.Model.ModelName(), e.Model.IDValue(), e.Err)
	}

	return fmt.Sprintf("query error: %s", e.Err)
}

type ErrScan struct {
	Err error
}

func (e *ErrScan) Error() string {
	return fmt.Sprintf("scan error: %s", e.Err)
}
