package lyfwdriver_test

import (
	"context"
	"log"
	"testing"
	"time"

	"github.com/magiconair/properties/assert"

	"gitlab.com/nshibalov/go-lyfw/example/test/model"
	"gitlab.com/nshibalov/go-lyfw/example/test/model/outer"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwdriver"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

type TestDriver struct {
	lyfwdriver.Dummy
}

func (b *TestDriver) Query(ctx context.Context, tree *lyfwstore.Node) <-chan lyfwstore.DriverResult {
	log.Printf("TestDriver: query %+v", tree)

	resultChan := make(chan lyfwstore.DriverResult)

	go func() {
		defer close(resultChan)

		time.Sleep(25 * time.Millisecond)

		if tree.Params().Filter.Value != 626 {
			resultChan <- lyfwstore.NewDriverResultError(&lyfwstore.ErrNotFound{Model: tree.Model})
			return
		}

		v := tree.Model.New()

		if err := v.SetFieldValue("ID", 626); err != nil {
			resultChan <- lyfwstore.NewDriverResultError(err)
			return
		}

		if err := v.SetFieldValue("Name", "Qwerty"); err != nil {
			resultChan <- lyfwstore.NewDriverResultError(err)
			return
		}

		resultChan <- lyfwstore.DriverResult{Node: tree, Value: v}
	}()

	return resultChan
}

type DummyTestListener struct{}

func (l *DummyTestListener) BeforeQuery(ctx context.Context, params *lyfwstore.Params, m lyfwstore.Model, fields *[]string, filter *lyfwstore.Filter) error {
	log.Print(params, m, fields, filter)
	return nil
}

func (l *DummyTestListener) AfterQuery(ctx context.Context, params *lyfwstore.Params, v lyfwstore.Model, fields []string, filter lyfwstore.Filter) {
	log.Print(params, v, fields, filter)
}

func (l *DummyTestListener) BeforeCreate(ctx context.Context, v lyfwstore.Model) error {
	log.Print(v)
	return nil
}

func (l *DummyTestListener) AfterCreate(ctx context.Context, v lyfwstore.Model) error {
	log.Print(v)
	return nil
}

func TestDummyDriverGet(t *testing.T) {
	user := model.User{Common: outer.Common{ID: 626}}

	store := lyfwstore.NewStore(&TestDriver{})
	store.SubscribeToQuery(&DummyTestListener{}, &user)

	err := store.Get(context.Background(), &user)

	assert.Equal(t, err, nil)
	assert.Equal(t, user.Name, "Qwerty")
}

func TestDummyDriverGetFail(t *testing.T) {
	user := model.User{Common: outer.Common{ID: 627}}

	store := lyfwstore.NewStore(&TestDriver{})
	store.SubscribeToQuery(&DummyTestListener{}, &user)

	err := store.Get(context.Background(), &user)

	assert.Equal(t, err, &lyfwstore.ErrNotFound{Model: &user})
}

func TestDummyDriverCreate(t *testing.T) {
	user := model.User{Common: outer.Common{ID: 626}}

	store := lyfwstore.NewStore(&TestDriver{})
	store.SubscribeToCreate(&DummyTestListener{}, &user)

	err := store.Create(context.Background(), &user)

	assert.Equal(t, err, nil)
}
