package lyfwservice

import (
	"go/types"
	"text/template"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwgen"
)

const (
	describerName     = "service"
	describerTemplate = "service.txt"

	describerFuncMethods = "methods"
)

type describerFuncs struct {
	h lyfwgen.TypesHelper
}

func (df *describerFuncs) methods(obj types.Object) (structMethods []*types.Func) {
	df.h.IterStructMethods(obj.Type(), func(method *types.Func) {
		structMethods = append(structMethods, method)
	})

	return
}

func NewDescriber(templatesDirPathPrefix string) (lyfwgen.Describer, error) {
	df := describerFuncs{}
	return lyfwgen.NewDescriber(describerName, describerTemplate, templatesDirPathPrefix, template.FuncMap{
		describerFuncMethods: df.methods,
	})
}
