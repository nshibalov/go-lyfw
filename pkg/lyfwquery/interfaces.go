package lyfwquery

import "gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"

type SQLGenerator interface {
	NewForNode(root *lyfwstore.Node) SQLGenerator
	GenSelect(count bool) (query string, values []interface{}, err error)
}
