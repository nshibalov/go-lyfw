package lyfwquery_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nshibalov/go-lyfw/example/test/model"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwquery"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

func TestPGSQLGenerator(t *testing.T) {
	params := lyfwstore.Params{
		Fields: []string{"user.name"},
		Filter: lyfwstore.Filter{
			Operator: lyfwstore.OR,
			Filters: []lyfwstore.Filter{
				{
					Field:    "addressLine1",
					Operator: lyfwstore.EQ,
					Value:    "asdfgh",
				},
				{
					Field:    "user.name",
					Operator: lyfwstore.NEQ,
					Value:    "qwerty",
				},
			},
		},
		Limit:  10,
		Offset: 150,
	}

	err := params.Translate(&model.Address{})
	if err != nil {
		t.Fatal(err)
	}

	root, err := lyfwstore.BuildNodeTree(&params)
	if err != nil {
		t.Fatal(err)
	}

	g := lyfwquery.NewPGSQLGenerator(root, lyfwstore.ToSnakeCase)

	s, v, err := g.GenSelect(false)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, `SELECT "t0"."id" AS "t0.id", "t0"."address_line_1" AS "t0.address_line_1", "t1"."id" AS "t1.id", "t1"."name" AS "t1.name" FROM "addresses" AS "t0" JOIN "users" AS "t1" ON "t1"."id" = "t0"."user_id" WHERE (("t0"."address_line_1" = $1 OR "t1"."name" <> $2)) LIMIT 10 OFFSET 150`, s)
	assert.Equal(t, []interface{}{"asdfgh", "qwerty"}, v)
}

func TestPGSQLGeneratorCount(t *testing.T) {
	params := lyfwstore.Params{
		Fields: []string{"user.name"},
		Filter: lyfwstore.Filter{
			Operator: lyfwstore.OR,
			Filters: []lyfwstore.Filter{
				{
					Field:    "addressLine1",
					Operator: lyfwstore.EQ,
					Value:    "asdfgh",
				},
				{
					Field:    "user.name",
					Operator: lyfwstore.NEQ,
					Value:    "qwerty",
				},
			},
		},
		Limit:  10,
		Offset: 150,
	}

	err := params.Translate(&model.Address{})
	if err != nil {
		t.Fatal(err)
	}

	root, err := lyfwstore.BuildNodeTree(&params)
	if err != nil {
		t.Fatal(err)
	}

	g := lyfwquery.NewPGSQLGenerator(root, lyfwstore.ToSnakeCase)

	s, v, err := g.GenSelect(true)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, `SELECT COUNT(*) FROM "addresses" AS "t0" JOIN "users" AS "t1" ON "t1"."id" = "t0"."user_id" WHERE (("t0"."address_line_1" = $1 OR "t1"."name" <> $2))`, s)
	assert.Equal(t, []interface{}{"asdfgh", "qwerty"}, v)
}

func TestPGSQLGeneratorManyFilterFail(t *testing.T) {
	params := lyfwstore.Params{
		Fields: []string{"users.name", "users.addresses.addressLine1"},
		Filter: lyfwstore.Filter{
			Operator: lyfwstore.OR,
			Filters: []lyfwstore.Filter{
				{
					Field:    "users.addresses.addressLine1",
					Operator: lyfwstore.EQ,
					Value:    "asdfgh",
				},
				{
					Field:    "name",
					Operator: lyfwstore.NEQ,
					Value:    "qwerty",
				},
			},
		},
		Limit:  10,
		Offset: 150,
	}

	err := params.Translate(&model.City{})
	if err != nil {
		t.Fatal(err)
	}

	root, err := lyfwstore.BuildNodeTree(&params)
	if err != nil {
		t.Fatal(err)
	}

	g := lyfwquery.NewPGSQLGenerator(root, lyfwstore.ToSnakeCase)

	_, _, err = g.GenSelect(false)

	assert.Equal(
		t,
		&lyfwquery.ErrFilterManyRelationField{
			Model: &model.Address{},
			Field: "Users.Addresses.AddressLine1",
		},
		err)
}

func TestPGSQLGeneratorMany(t *testing.T) {
	params := lyfwstore.Params{
		Fields: []string{"users.name", "users.addresses.addressLine1"},
		Filter: lyfwstore.Filter{
			Operator: lyfwstore.OR,
			Filters: []lyfwstore.Filter{
				{
					Field:    "name",
					Operator: lyfwstore.NEQ,
					Value:    "qwerty",
				},
			},
		},
		OrderBy: []lyfwstore.OrderBy{
			{Field: "name"},
		},
		Limit:  10,
		Offset: 150,
	}

	err := params.Translate(&model.City{})
	if err != nil {
		t.Fatal(err)
	}

	root, err := lyfwstore.BuildNodeTree(&params)
	if err != nil {
		t.Fatal(err)
	}

	g := lyfwquery.NewPGSQLGenerator(root, lyfwstore.ToSnakeCase)

	s, v, err := g.GenSelect(false)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, `SELECT "t1"."id" AS "t1.id", "t1"."name" AS "t1.name", "t2"."id" AS "t2.id", "t2"."address_line_1" AS "t2.address_line_1", "m".* FROM (SELECT "t0"."id" AS "t0.id", "t0"."title" AS "t0.title" FROM "cities" AS "t0" WHERE (("t0"."title" <> $1)) ORDER BY "t0"."title" ASC LIMIT 10 OFFSET 150) AS "m" JOIN "city_to_users" AS "t1_city_to_users" ON "t1_city_to_users"."city_id" = "t0.id" JOIN "users" AS "t1" ON "t1"."id" = "t1_city_to_users"."user_id" JOIN "addresses" AS "t2" ON "t2"."user_id" = "t1"."id" ORDER BY "t0.title" ASC`, s)
	assert.Equal(t, []interface{}{"qwerty"}, v)
}
