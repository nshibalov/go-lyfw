package lyfwquery

import (
	"fmt"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

type ErrOperatorNotSupported struct {
	Operator lyfwstore.Operator
}

func (e *ErrOperatorNotSupported) Error() string {
	return fmt.Sprintf("operator %s is not supported", e.Operator)
}

type ErrFilterManyRelationField struct {
	Model lyfwstore.Model
	Field string
}

func (e *ErrFilterManyRelationField) Error() string {
	return fmt.Sprintf("field %s (model %s) can't be filtered: filter on many* relations not supported", e.Field, e.Model.ModelName())
}

type ErrFieldNotSelected struct {
	Model lyfwstore.Model
	Field string
}

func (e *ErrFieldNotSelected) Error() string {
	return fmt.Sprintf("field %s.%s must be selected, when many* relations used in query", e.Model.ModelName(), e.Field)
}

type ErrNilRoot struct {
	Model lyfwstore.Model
	Field string
}

func (e *ErrNilRoot) Error() string {
	return "root is nil"
}
