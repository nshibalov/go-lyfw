package lyfwquery

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

type PGSQLGenerator struct {
	root  *lyfwstore.Node
	argID int

	hasManyNodes map[*lyfwstore.Node]bool

	nameMapper NameMapper
}

func (g *PGSQLGenerator) iterNodes(isManyRelations bool, callback func(node *lyfwstore.Node) error) error {
	return g.root.Iter(func(node *lyfwstore.Node) error {
		if isManyRelations {
			if !g.isManyRelation(node) {
				return nil
			}
		} else if g.isManyRelation(node) {
			return nil
		}

		return callback(node)
	})
}

func (g *PGSQLGenerator) isManyRelation(node *lyfwstore.Node) bool {
	return g.hasManyNodes[node]
}

func (g *PGSQLGenerator) hasManyRelations() bool {
	return len(g.hasManyNodes) > 0
}

func (g *PGSQLGenerator) writeColumns(w io.Writer, isManyRelations bool) bool {
	var columns []string

	_ = g.iterNodes(isManyRelations, func(node *lyfwstore.Node) error {
		for _, field := range node.Fields {
			column := g.fieldToColumn(node.Model, field)
			columns = append(columns, fmt.Sprintf(
				`"%s"."%s" AS "%s.%s"`,
				node.Alias, column,
				node.Alias, column,
			))
		}

		return nil
	})

	fmt.Fprint(w, strings.Join(columns, ", "))

	return len(columns) > 0
}

func (g *PGSQLGenerator) writeJoins(w io.Writer, isManyRelations bool) error {
	return g.iterNodes(isManyRelations, func(node *lyfwstore.Node) error {
		parent := node.Parent
		if parent == nil {
			return nil
		}

		parentIDColumn := g.fieldToColumn(parent.Model, parent.Model.IDField())
		nodeIDColumn := g.fieldToColumn(node.Model, node.Model.IDField())
		parentColumnAlias := g.isManyRelation(node) && !g.isManyRelation(parent)

		switch parent.Model.RelationType(node.RelationField) {
		case lyfwstore.RelationNone:
			return nil
		case lyfwstore.RelationHasOne:
			_, field := parent.Model.Relation(node.RelationField)
			column := g.fieldToColumn(parent.Model, field)

			var parentColumn string
			if parentColumnAlias {
				parentColumn = fmt.Sprintf(`"%s.%s"`, parent.Alias, column)
			} else {
				parentColumn = fmt.Sprintf(`"%s"."%s"`, parent.Alias, column)
			}

			fmt.Fprintf(
				w,
				` JOIN "%s" AS "%s" ON "%s"."%s" = %s`,
				node.Model.StoreName(), node.Alias,
				node.Alias, nodeIDColumn,
				parentColumn,
			)
		case lyfwstore.RelationHasMany:
			_, field := parent.Model.Relation(node.RelationField)
			column := g.fieldToColumn(node.Model, field)

			var parentColumn string
			if parentColumnAlias {
				parentColumn = fmt.Sprintf(`"%s.%s"`, parent.Alias, parentIDColumn)
			} else {
				parentColumn = fmt.Sprintf(`"%s"."%s"`, parent.Alias, parentIDColumn)
			}

			fmt.Fprintf(
				w,
				` JOIN "%s" AS "%s" ON "%s"."%s" = %s`,
				node.Model.StoreName(), node.Alias,
				node.Alias, column,
				parentColumn,
			)
		case lyfwstore.RelationManyToMany:
			m2mTable, sourceColumn, targetColumn := parent.Model.RelationManyToMany(node.RelationField)
			m2mAlias := fmt.Sprintf(`%s_%s`, node.Alias, m2mTable)

			var parentColumn string
			if parentColumnAlias {
				parentColumn = fmt.Sprintf(`"%s.%s"`, parent.Alias, parentIDColumn)
			} else {
				parentColumn = fmt.Sprintf(`"%s"."%s"`, parent.Alias, parentIDColumn)
			}

			fmt.Fprintf(
				w,
				` JOIN "%s" AS "%s" ON "%s"."%s" = %s JOIN "%s" AS "%s" ON "%s"."%s" = "%s"."%s"`,
				m2mTable, m2mAlias,
				m2mAlias, sourceColumn,
				parentColumn,
				node.Model.StoreName(), node.Alias,
				node.Alias, nodeIDColumn,
				m2mAlias, targetColumn,
			)
		}

		return nil
	})
}

func (g *PGSQLGenerator) filterOpInfo(op lyfwstore.Operator) (name, placeholderTemplate string, err error) {
	defTemplate := "$%d"
	inTemplate := "($%d)"

	switch op {
	case lyfwstore.EQ:
		return "=", defTemplate, nil
	case lyfwstore.NEQ:
		return "<>", defTemplate, nil
	case lyfwstore.GT:
		return ">", defTemplate, nil
	case lyfwstore.GTE:
		return ">=", defTemplate, nil
	case lyfwstore.LT:
		return "<", defTemplate, nil
	case lyfwstore.LTE:
		return "<=", defTemplate, nil
	case lyfwstore.ISNULL:
		return "IS NULL", "", nil
	case lyfwstore.ISNOTNULL:
		return "IS NOT NULL", "", nil
	case lyfwstore.IN:
		return "IN", inTemplate, nil
	case lyfwstore.NOTIN:
		return "NOT IN", inTemplate, nil
	}

	return "", "", &ErrOperatorNotSupported{Operator: op}
}

func (g *PGSQLGenerator) processFilter(filter *lyfwstore.Filter, values *[]interface{}) (string, error) {
	if filter.IsGroup() {
		var parts []string

		for _, f := range filter.Filters {
			lf := f

			fStr, err := g.processFilter(&lf, values)
			if err != nil {
				return "", err
			}

			if fStr != "" {
				parts = append(parts, fStr)
			}
		}

		if filter.Operator == lyfwstore.OR {
			return fmt.Sprintf(`(%s)`, strings.Join(parts, ` OR `)), nil
		}

		return fmt.Sprintf(`(%s)`, strings.Join(parts, ` AND `)), nil
	}

	if !filter.IsValid() {
		return "", nil
	}

	node, _ := g.root.GetNode(filter.Field)

	if g.isManyRelation(node) {
		return "", &ErrFilterManyRelationField{Model: node.Model, Field: filter.Field}
	}

	field := filter.Field
	if strings.Contains(field, ".") {
		field = field[strings.LastIndex(field, ".")+1:]
	}

	operator, placeholderTemplate, err := g.filterOpInfo(filter.Operator)
	if err != nil {
		return "", err
	}

	column := fmt.Sprintf(`"%s"."%s"`, node.Alias, g.fieldToColumn(node.Model, field))

	if placeholderTemplate == "" {
		// no value needed
		return fmt.Sprintf(`%s %s`, column, operator), nil
	}

	*values = append(*values, filter.Value)

	g.argID++

	return fmt.Sprintf(`%s %s %s`, column, operator, fmt.Sprintf(placeholderTemplate, g.argID)), nil
}

func (g *PGSQLGenerator) writeWhere(w io.Writer, values *[]interface{}) error {
	var mustFilter lyfwstore.Filter

	_ = g.iterNodes(false, func(node *lyfwstore.Node) error {
		if node.Parent == nil {
			return nil
		}

		if node.Filter.IsValid() {
			filter := node.Filter
			filter.Field = fmt.Sprintf("%s.%s", node.Path, filter.Field)

			mustFilter.Filters = append(mustFilter.Filters, filter)
		}

		return nil
	})

	res, err := g.processFilter(&lyfwstore.Filter{
		Operator: lyfwstore.AND,
		Filters: []lyfwstore.Filter{
			mustFilter,
			g.root.Params().Filter,
		},
	}, values)

	if err != nil {
		return err
	}

	if res != "" {
		// nolint:gosec // it's ok for generator
		fmt.Fprintf(w, ` WHERE %s`, res)
	}

	return nil
}

func (g *PGSQLGenerator) writeOrderBy(w io.Writer, alias bool) error {
	columns := make([]string, 0, len(g.root.Params().OrderBy))

	for _, orderBy := range g.root.Params().OrderBy {
		node, _ := g.root.GetNode(orderBy.Field)

		field := orderBy.Field
		if strings.Contains(field, ".") {
			field = field[strings.LastIndex(field, ".")+1:]
		}

		var column string

		if alias && !g.isManyRelation(node) {
			isInFields := false
			for _, f := range node.Fields {
				if f == field {
					isInFields = true
					break
				}
			}

			if !isInFields {
				return &ErrFieldNotSelected{Model: node.Model, Field: orderBy.Field}
			}

			column = fmt.Sprintf(`"%s.%s"`, node.Alias, g.fieldToColumn(node.Model, field))
		} else {
			column = fmt.Sprintf(`"%s"."%s"`, node.Alias, g.fieldToColumn(node.Model, field))
		}

		if orderBy.Descending {
			column = fmt.Sprintf(`%s DESC`, column)
		} else {
			column = fmt.Sprintf(`%s ASC`, column)
		}

		columns = append(columns, column)
	}

	if len(columns) > 0 {
		fmt.Fprintf(w, ` ORDER BY %s`, strings.Join(columns, ", "))
	}

	return nil
}

func (g *PGSQLGenerator) writeLimitOffset(w io.Writer) {
	params := g.root.Params()

	if params.Limit > 0 {
		fmt.Fprintf(w, ` LIMIT %d`, params.Limit)

		if params.Offset > 0 {
			fmt.Fprintf(w, ` OFFSET %d`, params.Offset)
		}
	}
}

func (g *PGSQLGenerator) genSelect(count bool) (query string, values []interface{}, err error) {
	sb := strings.Builder{}
	values = make([]interface{}, 0)

	fmt.Fprintf(&sb, `SELECT `)

	if count {
		fmt.Fprint(&sb, `COUNT(*)`)
	} else {
		g.writeColumns(&sb, false)
	}

	// nolint:gosec // it's ok for generator
	fmt.Fprintf(&sb, ` FROM "%s" AS "%s"`, g.root.Model.StoreName(), g.root.Alias)

	if err = g.writeJoins(&sb, false); err != nil {
		return
	}

	if err = g.writeWhere(&sb, &values); err != nil {
		return
	}

	if err = g.writeOrderBy(&sb, false); err != nil {
		return
	}

	if !count {
		g.writeLimitOffset(&sb)
	}

	query = sb.String()

	return
}

func (g *PGSQLGenerator) genSelectMany(count bool) (query string, values []interface{}, err error) {
	sb := strings.Builder{}

	query, values, err = g.genSelect(false)
	if err != nil {
		return
	}

	fmt.Fprintf(&sb, `SELECT `)

	if count {
		// nolint:gosec // it's ok for generator
		fmt.Fprintf(&sb, `COUNT(*) FROM (%s) AS "m"`, query)
	} else {
		if g.writeColumns(&sb, true) {
			fmt.Fprint(&sb, `, `)
		}

		// nolint:gosec // it's ok for generator
		fmt.Fprintf(&sb, `"m".* FROM (%s) AS "m"`, query)
	}

	if err = g.writeJoins(&sb, true); err != nil {
		return
	}

	if err = g.writeOrderBy(&sb, true); err != nil {
		return
	}

	query = sb.String()

	return
}

func (g *PGSQLGenerator) fieldToColumn(m lyfwstore.Model, field string) string {
	if g.nameMapper == nil {
		return m.FieldStoreName(field)
	}

	return g.nameMapper(m.FieldStoreName(field))
}

func (g PGSQLGenerator) NewForNode(root *lyfwstore.Node) SQLGenerator {
	n := PGSQLGenerator{
		root:         root,
		nameMapper:   g.nameMapper,
		hasManyNodes: make(map[*lyfwstore.Node]bool),
	}

	_ = root.Iter(func(node *lyfwstore.Node) error {
		if node.Parent == nil {
			return nil
		}

		if n.isManyRelation(node.Parent) {
			n.hasManyNodes[node] = true
			return nil
		}

		switch node.Parent.Model.RelationType(node.RelationField) {
		case lyfwstore.RelationHasMany, lyfwstore.RelationManyToMany:
			n.hasManyNodes[node] = true
		}

		return nil
	})

	return &n
}

func (g *PGSQLGenerator) GenSelect(count bool) (query string, values []interface{}, err error) {
	if g.root == nil {
		return "", nil, &ErrNilRoot{}
	}

	if count {
		return g.genSelect(count)
	}

	if g.hasManyRelations() {
		return g.genSelectMany(count)
	}

	return g.genSelect(count)
}

func NewPGSQLGenerator(root *lyfwstore.Node, nameMapper NameMapper) SQLGenerator {
	return PGSQLGenerator{nameMapper: nameMapper}.NewForNode(root)
}
