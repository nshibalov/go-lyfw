package lyfwstore

import (
	"fmt"
	"go/types"
	"log"
	"regexp"
	"strings"
	"text/template"

	"github.com/fatih/structtag"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwgen"
)

const (
	describerName     = "store"
	describerTemplate = "store.txt"

	describerFuncStoreName      = "storeName"
	describerFuncImports        = "imports"
	describerFuncFields         = "fields"
	describerFuncRelationFields = "relationFields"
)

var (
	describerStoreNameRx   = regexp.MustCompile(fmt.Sprintf(`%s:\s*(\S+)`, DocStoreName))
	describerExternalTagRx = regexp.MustCompile(fmt.Sprintf(`%s:\s*(\S+)`, DocExternalTag))
)

type describerField struct {
	Name,
	StoreName,
	ExternalName string

	IsID bool

	RawType string

	RelationType,
	RelationField,
	RelationModel string

	RelationTable,
	RelationSourceColumn,
	RelationTargetColumn string
}

type describerFuncs struct {
	h lyfwgen.TypesHelper
}

func (df *describerFuncs) storeName(obj types.Object, comment string) string {
	var storeName string

	if m := describerStoreNameRx.FindStringSubmatch(comment); m != nil {
		storeName = m[1]
	} else {
		storeName = strings.ToLower(obj.Name())
	}

	return storeName
}

func (df *describerFuncs) imports(obj types.Object) map[string]string {
	imports := map[string]string{}

	df.h.IterStructFields(obj.Type(), func(field *types.Var, tags *structtag.Tags) {
		dPkgID := df.h.GetTypePkgID(obj.Type())
		fPkgID := df.h.GetTypePkgID(field.Type())
		if fPkgID != dPkgID && fPkgID != "" {
			imports[fPkgID] = ""
		}
	})

	return imports
}

func (df *describerFuncs) fields(obj types.Object, comment string) (structFields []describerField) {
	externalTag := TagJSON
	if m := describerExternalTagRx.FindStringSubmatch(comment); m != nil {
		externalTag = m[1]
	}

	hasID := false

	df.h.IterStructFields(obj.Type(), func(field *types.Var, tags *structtag.Tags) {
		desc := describerField{
			Name:         field.Name(),
			StoreName:    field.Name(),
			ExternalName: field.Name(),
			RawType:      df.h.GetRawTypeName(field.Type(), obj.Pkg()),
		}

		if tag, _ := tags.Get(TagStore); tag != nil {
			desc.StoreName = tag.Name
		}

		if tag, _ := tags.Get(externalTag); tag != nil {
			desc.ExternalName = tag.Name
		}

		if tag, _ := tags.Get(TagID); tag != nil {
			desc.IsID = true
			hasID = true
		}

		if tag, _ := tags.Get(TagRel); tag == nil {
			structFields = append(structFields, desc)
		}
	})

	if !hasID {
		log.Fatalf("model %s doesn't have an ID field", obj.Name())
	}

	return
}

func (df *describerFuncs) relationFields(obj types.Object, comment string) (structFields []describerField) {
	externalTag := TagJSON
	if m := describerExternalTagRx.FindStringSubmatch(comment); m != nil {
		externalTag = m[1]
	}

	df.h.IterStructFields(obj.Type(), func(field *types.Var, tags *structtag.Tags) {
		tag, _ := tags.Get(TagRel)
		if tag == nil {
			return
		}

		desc := describerField{
			Name:          field.Name(),
			ExternalName:  field.Name(),
			RelationModel: df.h.GetTypeName(field.Type(), obj.Pkg()),
		}

		if len(tag.Options) > 0 {
			desc.RelationField = tag.Options[0]
		}

		if extTag, _ := tags.Get(externalTag); extTag != nil {
			desc.ExternalName = extTag.Name
		}

		switch tag.Name {
		case RelationNameHasOne:
			desc.RelationType = "RelationHasOne"
			desc.RelationField = df.h.GetTypeName(field.Type(), nil) + "ID"
		case RelationNameHasMany:
			desc.RelationType = "RelationHasMany"
			desc.RelationField = obj.Name() + "ID"
		case RelationNameManyToMany:
			desc.RelationType = "RelationManyToMany"
			desc.RelationField = ""

			const optsCount = 3
			if len(tag.Options) != optsCount {
				log.Fatalf(
					"%s relation tag format: <table name>,<source_column>,<target column>, got: %s",
					RelationNameManyToMany,
					fmt.Sprintf("<%s>", strings.Join(tag.Options, ">,<")))

				break
			}

			desc.RelationTable, desc.RelationSourceColumn, desc.RelationTargetColumn = tag.Options[0], tag.Options[1], tag.Options[2]
		}

		structFields = append(structFields, desc)
	})

	return
}

func NewDescriber(pathPrefix string) (lyfwgen.Describer, error) {
	df := describerFuncs{}
	return lyfwgen.NewDescriber(describerName, describerTemplate, pathPrefix, template.FuncMap{
		describerFuncStoreName:      df.storeName,
		describerFuncImports:        df.imports,
		describerFuncFields:         df.fields,
		describerFuncRelationFields: df.relationFields,
	})
}
