package lyfwstore_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nshibalov/go-lyfw/example/test/model"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

func TestParamsTranslate(t *testing.T) {
	params := lyfwstore.Params{
		Fields: []string{"users.addresses.user.order.name"},
	}

	err := params.Translate(&model.City{})
	if err != nil {
		t.Error(err)
		return
	}

	assert.Equal(t, "Users.Addresses.User.Order.Name", params.Fields[0])
}
