package lyfwstore

import (
	"fmt"
)

type ErrNotFound struct {
	Model Model
}

func (e *ErrNotFound) Error() string {
	return fmt.Sprintf("model %s with id '%s' not found", e.Model.ModelName(), e.Model.IDValue())
}

type ErrMultipleResultFound struct {
	Model Model
}

func (e *ErrMultipleResultFound) Error() string {
	return fmt.Sprintf("multiple result for model %s with id '%s'", e.Model.ModelName(), e.Model.IDValue())
}

type ErrWrongType struct {
	Value interface{}
}

func (e *ErrWrongType) Error() string {
	return fmt.Sprintf("wrong type: %T", e.Value)
}

type ErrUnknownExternalField struct {
	Model Model
	Field string
}

func (e *ErrUnknownExternalField) Error() string {
	return fmt.Sprintf("model %s: unknown external field name '%s'", e.Model.ModelName(), e.Field)
}

type ErrParamsNotTranslated struct {
}

func (e *ErrParamsNotTranslated) Error() string {
	return "params is not translated"
}

type ErrParamsAlreadyTranslated struct {
	Model Model
	Field string
}

func (e *ErrParamsAlreadyTranslated) Error() string {
	return fmt.Sprintf("params already translated for model %s", e.Model.ModelName())
}

type ErrNonRelationExternalField struct {
	Model       Model
	Path        string
	PartialPath string
	Field       string
}

func (e *ErrNonRelationExternalField) Error() string {
	return fmt.Sprintf("model %s: external field '%s' contains non-relation part %s.%s",
		e.Model.ModelName(),
		e.Path,
		e.PartialPath,
		e.Field)
}
