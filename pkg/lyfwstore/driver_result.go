package lyfwstore

type DriverResult struct {
	Node  *Node
	Value Model
	Error error
}

func NewDriverResultError(err error) DriverResult {
	return DriverResult{Error: err}
}
