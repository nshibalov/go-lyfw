package lyfwstore

import (
	"context"
)

type Model interface {
	ModelName() string // model name, e.g. User
	StoreName() string // name in store, e.g. users

	FieldNames() []string
	HasField(field string) bool
	RelationFieldNames() []string

	FieldStoreName(field string) string
	FieldExternalName(field string) string

	FieldValue(field string) interface{}
	FieldValuePtr(field string) interface{}
	SetFieldValue(field string, value interface{}) error

	RelationType(field string) RelationType
	Relation(field string) (model Model, relField string)
	RelationManyToMany(field string) (table, sourceColumn, targetColumn string)

	New() Model

	IDField() string
	IDValue() interface{}

	SetRelation(field string, v ...Model)
}

type Storer interface {
	Get(ctx context.Context, m Model) error
	Create(ctx context.Context, m Model) error
	Update(ctx context.Context, m Model, fields ...string) error
	Delete(ctx context.Context, m Model) error
	Query(ctx context.Context, params *Params) <-chan QueryResult

	SubscribeToCreate(l CreateListener, m ...Model)
	SubscribeToUpdate(l UpdateListener, m ...Model)
	SubscribeToDelete(l DeleteListener, m ...Model)
	SubscribeToQuery(l QueryListener, m ...Model)
}

type Driver interface {
	Begin(ctx context.Context) (context.Context, error)
	Commit(ctx context.Context) error
	Rollback(ctx context.Context) error

	Create(ctx context.Context, m Model) error
	Update(ctx context.Context, m Model, fields ...string) error
	Delete(ctx context.Context, m Model) error
	Query(ctx context.Context, tree *Node) <-chan DriverResult
}

type CreateListener interface {
	BeforeCreate(ctx context.Context, m Model) error
	AfterCreate(ctx context.Context, m Model) error
}

type UpdateListener interface {
	BeforeUpdate(ctx context.Context, m Model, fields *[]string) error
	AfterUpdate(ctx context.Context, m Model, fields []string) error
}

type DeleteListener interface {
	BeforeDelete(ctx context.Context, m Model) error
	AfterDelete(ctx context.Context, m Model) error
}

type QueryListener interface {
	BeforeQuery(ctx context.Context, params *Params, m Model, fields *[]string, filter *Filter) error
	AfterQuery(ctx context.Context, params *Params, m Model, fields []string, filter Filter)
}
