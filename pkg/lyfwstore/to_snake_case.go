package lyfwstore

import (
	"strings"
	"unicode"
)

func ToSnakeCase(str string) string {
	if len(str) < 2 {
		return strings.ToLower(str)
	}

	runes := []rune(str)
	p := runes[len(runes)-1]
	ret := []rune{unicode.ToLower(p)}

	isCap := false

	for i := len(runes) - 2; i >= 0; i-- {
		c := runes[i]

		switch {
		case c == rune('_') || p == rune('_'):
		case unicode.IsUpper(c) && unicode.IsLower(p):
			isCap = true
		case isCap:
			isCap = false
			fallthrough
		case unicode.IsDigit(c) && !unicode.IsDigit(p),
			unicode.IsLetter(c) && !unicode.IsLetter(p),
			unicode.IsLower(c) && unicode.IsUpper(p):
			ret = append(ret, rune('_'))
		}

		ret = append(ret, unicode.ToLower(c))

		p = c
	}

	for i, j := 0, len(ret)-1; i < j; i, j = i+1, j-1 {
		ret[i], ret[j] = ret[j], ret[i]
	}

	return string(ret)
}
