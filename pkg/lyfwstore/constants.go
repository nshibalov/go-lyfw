package lyfwstore

// RelationType - Type of relation
type RelationType int

// Available relation types
const (
	RelationNone RelationType = iota
	RelationHasOne
	RelationHasMany
	RelationManyToMany
)

// Constants for generator control
const (
	TagRel  = "lyfw_rel"
	TagSkip = "lyfw_skip"
	TagID   = "lyfw_id"

	TagStore = "db"
	TagJSON  = "json"

	DocExternalTag = "lyfw_externalTag"
	DocStoreName   = "lyfw_storeName"
)

// Relation names
const (
	RelationNameNone       = "none"
	RelationNameHasOne     = "hasOne"
	RelationNameHasMany    = "hasMany"
	RelationNameManyToMany = "manyToMany"
)

// CtxKey - Type for Store context values
type CtxKey uint

const (
	// CtxInStore - This context value tells if we inside Store method or not
	// Needed for transaction control for case of subcalls to Store from Store
	CtxInStore CtxKey = iota
)
