package lyfwstore_test

import (
	"testing"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

func TestToSnakeCase(t *testing.T) {
	tests := []struct {
		input string
		want  string
	}{
		{"", ""},
		{"already_snake", "already_snake"},
		{"A", "a"},
		{"AA", "aa"},
		{"AaAa", "aa_aa"},
		{"HTTPRequest", "http_request"},
		{"BatteryLifeValue", "battery_life_value"},
		{"Id0Value", "id_0_value"},
		{"ID0Value", "id_0_value"},
		{"addressLine1", "address_line_1"},
		{"aa99BBcc", "aa_99_b_bcc"},
	}
	for _, test := range tests {
		have := lyfwstore.ToSnakeCase(test.input)
		if have != test.want {
			t.Errorf("input=%q:\nhave: %q\nwant: %q", test.input, have, test.want)
		}
	}
}
