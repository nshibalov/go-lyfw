package lyfwstore_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nshibalov/go-lyfw/example/test/model"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

func TestBuildNodeTree(t *testing.T) {
	params := lyfwstore.Params{
		Fields: []string{"users.addresses.user.order.name"},
		Filter: lyfwstore.Filter{
			Field:    "users.addresses.user.order.name",
			Operator: lyfwstore.EQ,
			Value:    "Qwerty",
		},
		OrderBy: []lyfwstore.OrderBy{
			{Field: "users.addresses.user.order.name", Descending: true},
		},
	}

	err := params.Translate(&model.City{})
	if err != nil {
		t.Fatal(err)
	}

	node, err := lyfwstore.BuildNodeTree(&params)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, "Users.Addresses.User.Order.Name", params.Fields[0])
	assert.Equal(t, "Users.Addresses.User.Order.Name", params.Filter.Field)
	assert.Equal(t, "Users.Addresses.User.Order.Name", params.OrderBy[0].Field)

	assert.Equal(t, node, node.Root())
	assert.Equal(t, "", node.Path)

	last := node.Nodes["Users"].Nodes["Addresses"].Nodes["User"].Nodes["Order"]

	assert.Equal(t, node, last.Root())
	assert.Equal(t, node.Params(), last.Params())
	assert.Equal(t, "Users.Addresses.User.Order", last.Path)
}

func TestBuildNodeTreeFail1(t *testing.T) {
	{
		_, err := lyfwstore.BuildNodeTree(&lyfwstore.Params{})
		assert.Equal(t, &lyfwstore.ErrParamsNotTranslated{}, err)
	}

	{
		params := lyfwstore.Params{
			Fields: []string{"users.name.user.order.name"},
		}

		err := params.Translate(&model.City{})
		assert.Equal(
			t,
			&lyfwstore.ErrNonRelationExternalField{
				Model:       &model.City{},
				Path:        "users.name.user.order.name",
				PartialPath: "Users",
				Field:       "Name",
			},
			err)
	}

	{
		params := lyfwstore.Params{
			Fields: []string{"users.addresses.user.order.name"},
		}

		_ = params.Translate(&model.City{})
		err := params.Translate(&model.City{})

		assert.Equal(t, &lyfwstore.ErrParamsAlreadyTranslated{Model: params.Model}, err)
	}

	{
		params := lyfwstore.Params{
			Fields: []string{"Users.Name.User.Order.Name"},
			Model:  &model.City{},
		}

		_, err := lyfwstore.BuildNodeTree(&params)

		assert.Equal(
			t,
			&lyfwstore.ErrNonRelationExternalField{
				Model:       params.Model,
				Path:        "Users.Name.User.Order.Name",
				PartialPath: "Users",
				Field:       "Name"},
			err)
	}
}
