package lyfwstore

type QueryResult struct {
	Value Model
	Error error
}

func NewQueryResultError(err error) QueryResult {
	return QueryResult{Error: err}
}

func NewQueryResultValue(value Model) QueryResult {
	return QueryResult{Value: value}
}
