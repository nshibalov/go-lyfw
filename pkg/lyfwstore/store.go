package lyfwstore

import (
	"context"
	"log"
)

type deferFunc func(err error, recoverValue interface{})

type store struct {
	driver Driver

	cSubscribers map[string][]CreateListener
	uSubscribers map[string][]UpdateListener
	dSubscribers map[string][]DeleteListener
	qSubscribers map[string][]QueryListener
}

func (s *store) begin(ctx context.Context) (context.Context, deferFunc, error) {
	if v, _ := ctx.Value(CtxInStore).(bool); v {
		// sub call, nothing to do
		return ctx, func(error, interface{}) {}, nil
	}

	ctx = context.WithValue(ctx, CtxInStore, true)
	ctx, err := s.driver.Begin(ctx)

	return ctx, func(err error, recoverValue interface{}) {
		switch {
		case recoverValue != nil, err != nil:
			if recoverValue != nil {
				log.Printf("panic in transaction: %s", recoverValue)
			} else if err != nil {
				log.Printf("error in transaction: %s", err)
			}

			if err := s.driver.Rollback(ctx); err != nil {
				log.Printf("failed to rollback: %s", err)
			}
		default:
			if err := s.driver.Commit(ctx); err != nil {
				log.Printf("failed to commit: %s", err)
			}
		}
	}, err
}

func (s *store) Get(ctx context.Context, m Model) error {
	resultChan := s.Query(ctx, &Params{Filter: Filter{Operator: EQ, Field: m.IDField(), Value: m.IDValue()}, Model: m})

	results := make([]Model, 0)

	for r := range resultChan {
		if r.Error != nil {
			return r.Error
		}

		results = append(results, r.Value)
	}

	if len(results) == 0 {
		return &ErrNotFound{Model: m}
	} else if len(results) > 1 {
		return &ErrMultipleResultFound{Model: m}
	}

	for _, f := range m.FieldNames() {
		if err := m.SetFieldValue(f, results[0].FieldValue(f)); err != nil {
			return err
		}
	}

	return nil
}

// nolint:dupl // this is NOT duplicate
func (s *store) Create(ctx context.Context, m Model) (err error) {
	ctx, deferFunc, err := s.begin(ctx)
	if err != nil {
		return
	}

	defer func() { deferFunc(err, recover()) }()

	listeners := append(s.cSubscribers[""], s.cSubscribers[m.ModelName()]...)

	for _, l := range listeners {
		if err = l.BeforeCreate(ctx, m); err != nil {
			return
		}
	}

	if err = s.driver.Create(ctx, m); err != nil {
		return
	}

	for _, l := range listeners {
		if err = l.AfterCreate(ctx, m); err != nil {
			return
		}
	}

	return
}

func (s *store) Update(ctx context.Context, m Model, fields ...string) (err error) {
	ctx, deferFunc, err := s.begin(ctx)
	if err != nil {
		return
	}

	defer func() { deferFunc(err, recover()) }()

	listeners := append(s.uSubscribers[""], s.uSubscribers[m.ModelName()]...)

	for _, l := range listeners {
		if err = l.BeforeUpdate(ctx, m, &fields); err != nil {
			return
		}
	}

	if err = s.driver.Update(ctx, m, fields...); err != nil {
		return
	}

	for _, l := range listeners {
		if err = l.AfterUpdate(ctx, m, fields); err != nil {
			return
		}
	}

	return
}

// nolint:dupl // this is NOT duplicate
func (s *store) Delete(ctx context.Context, m Model) (err error) {
	ctx, deferFunc, err := s.begin(ctx)
	if err != nil {
		return
	}

	defer func() { deferFunc(err, recover()) }()

	listeners := append(s.dSubscribers[""], s.dSubscribers[m.ModelName()]...)

	for _, l := range listeners {
		if err = l.BeforeDelete(ctx, m); err != nil {
			return
		}
	}

	if err = s.driver.Delete(ctx, m); err != nil {
		return
	}

	for _, l := range listeners {
		if err = l.AfterDelete(ctx, m); err != nil {
			return
		}
	}

	return
}

func (s *store) query(ctx context.Context, params *Params, resultChan chan<- QueryResult) {
	ctx, deferFunc, err := s.begin(ctx)
	if err != nil {
		return
	}

	defer func() {
		deferFunc(err, recover())

		if err != nil {
			resultChan <- NewQueryResultError(err)
		}

		close(resultChan)
	}()

	tree, err := BuildNodeTree(params)
	if err != nil {
		return
	}

	listeners := append(s.qSubscribers[""], s.qSubscribers[params.Model.ModelName()]...)

	err = tree.Iter(func(node *Node) error {
		for _, l := range listeners {
			if e := l.BeforeQuery(ctx, params, node.Model, &node.Fields, &node.Filter); e != nil {
				return e
			}
		}

		return nil
	})

	if err != nil {
		return
	}

	driverResultChan := s.driver.Query(ctx, tree)

	for r := range driverResultChan {
		if r.Error != nil {
			err = r.Error
			break
		}

		listeners := append(s.qSubscribers[""], s.qSubscribers[r.Value.ModelName()]...)
		for _, l := range listeners {
			l.AfterQuery(ctx, params, r.Value, r.Node.Fields, r.Node.Filter)
		}

		resultChan <- NewQueryResultValue(r.Value)
	}
}

func (s *store) Query(ctx context.Context, params *Params) <-chan QueryResult {
	resultChan := make(chan QueryResult)

	go s.query(ctx, params, resultChan)

	return resultChan
}

func (s *store) SubscribeToCreate(l CreateListener, m ...Model) {
	if len(m) == 0 {
		s.cSubscribers[""] = append(s.cSubscribers[""], l)
		return
	}

	for _, v := range m {
		k := v.ModelName()
		s.cSubscribers[k] = append(s.cSubscribers[k], l)
	}
}

func (s *store) SubscribeToUpdate(l UpdateListener, m ...Model) {
	if len(m) == 0 {
		s.uSubscribers[""] = append(s.uSubscribers[""], l)
		return
	}

	for _, v := range m {
		k := v.ModelName()
		s.uSubscribers[k] = append(s.uSubscribers[k], l)
	}
}

func (s *store) SubscribeToDelete(l DeleteListener, m ...Model) {
	if len(m) == 0 {
		s.dSubscribers[""] = append(s.dSubscribers[""], l)
		return
	}

	for _, v := range m {
		k := v.ModelName()
		s.dSubscribers[k] = append(s.dSubscribers[k], l)
	}
}

func (s *store) SubscribeToQuery(l QueryListener, m ...Model) {
	if len(m) == 0 {
		s.qSubscribers[""] = append(s.qSubscribers[""], l)
		return
	}

	for _, v := range m {
		k := v.ModelName()
		s.qSubscribers[k] = append(s.qSubscribers[k], l)
	}
}

func NewStore(driver Driver) Storer {
	return &store{
		driver:       driver,
		cSubscribers: make(map[string][]CreateListener),
		uSubscribers: make(map[string][]UpdateListener),
		dSubscribers: make(map[string][]DeleteListener),
		qSubscribers: make(map[string][]QueryListener),
	}
}
