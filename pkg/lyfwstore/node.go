package lyfwstore

import (
	"fmt"
	"strings"
)

type Node struct {
	Alias  string
	Model  Model
	Fields []string
	Nodes  map[string]*Node

	Filter Filter // must filter, additional to params filter

	Parent        *Node
	RelationField string
	Path          string

	params *Params
}

func (node *Node) Root() *Node {
	if node.Parent == nil {
		return node
	}

	return node.Parent.Root()
}

func (node *Node) Params() *Params {
	return node.Root().params
}

func (node *Node) Iter(callback func(*Node) error) error {
	if err := callback(node); err != nil {
		return err
	}

	for _, subNode := range node.Nodes {
		if err := subNode.Iter(callback); err != nil {
			return err
		}
	}

	return nil
}

func (node *Node) GetNode(field string) (*Node, error) {
	if !strings.Contains(field, ".") {
		return node, nil
	}

	Path := field[:strings.LastIndex(field, ".")]
	currentNode := node

	for _, f := range strings.Split(Path, ".") {
		if subNode, ok := currentNode.Nodes[f]; ok {
			currentNode = subNode
			continue
		}

		var fPath string
		if currentNode.Path == "" {
			fPath = f
		} else {
			fPath = fmt.Sprintf("%s.%s", currentNode.Path, f)
		}

		RelationType := currentNode.Model.RelationType(f)

		if RelationType == RelationNone {
			return nil, &ErrNonRelationExternalField{
				Model:       node.Model,
				Path:        field,
				PartialPath: currentNode.Path,
				Field:       f,
			}
		}

		model, _ := currentNode.Model.Relation(f)

		newNode := NewNode(model)
		newNode.Parent = currentNode
		newNode.RelationField = f
		newNode.Path = fPath

		currentNode.Nodes[f] = newNode
		currentNode = newNode
	}

	return currentNode, nil
}

func NewNode(model Model) *Node {
	return &Node{
		Model: model,
		Nodes: make(map[string]*Node),
	}
}

func BuildNodeTree(params *Params) (*Node, error) {
	if !params.Translated() {
		return nil, &ErrParamsNotTranslated{}
	}

	root := NewNode(params.Model)

	root.Fields = []string{params.Model.IDField()}
	root.params = params

	addField := func(field string) error {
		node, err := root.GetNode(field)
		if err != nil {
			return err
		}

		if strings.Contains(field, ".") {
			field = field[strings.LastIndex(field, ".")+1:]
		}

		for _, f := range node.Fields {
			if f == field {
				return nil
			}
		}

		if len(node.Fields) == 0 {
			node.Fields = append(node.Fields, node.Model.IDField())
		}

		node.Fields = append(node.Fields, field)

		return nil
	}

	for _, field := range params.Fields {
		if err := addField(field); err != nil {
			return nil, err
		}
	}

	for _, orderBy := range params.OrderBy {
		if err := addField(orderBy.Field); err != nil {
			return nil, err
		}
	}

	err := params.Filter.Iter(func(filter *Filter) error {
		return addField(filter.Field)
	})

	if err != nil {
		return nil, err
	}

	id := 0
	_ = root.Iter(func(node *Node) error {
		node.Alias = fmt.Sprintf("t%d", id)
		id++
		return nil
	})

	return root, nil
}
