package lyfwstore

import (
	"strings"
)

type Operator string

const (
	EQ        Operator = "$eq"
	NEQ       Operator = "$neq"
	GT        Operator = "$gt"
	GTE       Operator = "$gte"
	LT        Operator = "$lt"
	LTE       Operator = "$lte"
	ISNULL    Operator = "$isnull"
	ISNOTNULL Operator = "$isnotnull"
	IN        Operator = "$in"
	NOTIN     Operator = "$notin"
	AND       Operator = "$and"
	OR        Operator = "$or"
)

type OrderBy struct {
	Field      string `json:"field"`
	Descending bool   `json:"desc"`
}

type Filter struct {
	Operator Operator `json:"op"`

	Field string      `json:"field,omitempty"`
	Value interface{} `json:"value,omitempty"`

	Filters []Filter `json:"filters,omitempty"`
}

type Params struct {
	Fields  []string
	Filter  Filter
	OrderBy []OrderBy
	Model   Model
	Limit   uint
	Offset  uint
}

func (f *Filter) IsGroup() bool {
	return (f.Operator == AND || f.Operator == OR)
}

func (f *Filter) IsValid() bool {
	for i := range f.Filters {
		if !f.Filters[i].IsValid() {
			return false
		}
	}

	if f.IsGroup() {
		return true
	}

	return f.Field != ""
}

func (f *Filter) Iter(callback func(*Filter) error) error {
	if len(f.Filters) == 0 && f.Field != "" {
		if err := callback(f); err != nil {
			return err
		}
	}

	for i := range f.Filters {
		filter := &f.Filters[i]
		if err := filter.Iter(callback); err != nil {
			return err
		}
	}

	return nil
}

func (p *Params) translateField(model Model, field string) (string, error) {
	for _, fieldName := range model.FieldNames() {
		extFieldName := model.FieldExternalName(fieldName)
		if extFieldName == field {
			return fieldName, nil
		}
	}

	for _, fieldName := range model.RelationFieldNames() {
		extFieldName := model.FieldExternalName(fieldName)
		if extFieldName == field {
			return fieldName, nil
		}
	}

	return "", &ErrUnknownExternalField{Model: model, Field: field}
}

func (p *Params) translateFieldPath(model Model, fieldPath string) (string, error) {
	path := strings.Split(fieldPath, ".")
	m := model

	for i, f := range path {
		tr, err := p.translateField(m, f)
		if err != nil {
			return "", err
		}

		if m.RelationType(tr) == RelationNone && i+1 != len(path) {
			return "", &ErrNonRelationExternalField{
				Model:       model,
				Path:        fieldPath,
				PartialPath: strings.Join(path[:i], "."),
				Field:       tr,
			}
		}

		path[i] = tr

		m, _ = m.Relation(tr)
	}

	return strings.Join(path, "."), nil
}

func (p *Params) Translate(model Model) error {
	if p.Model != nil {
		return &ErrParamsAlreadyTranslated{Model: p.Model}
	}

	for i, field := range p.Fields {
		tr, err := p.translateFieldPath(model, field)
		if err != nil {
			return err
		}

		p.Fields[i] = tr
	}

	for i, orderBy := range p.OrderBy {
		tr, err := p.translateFieldPath(model, orderBy.Field)
		if err != nil {
			return err
		}

		p.OrderBy[i].Field = tr
	}

	err := p.Filter.Iter(func(filter *Filter) error {
		tr, err := p.translateFieldPath(model, filter.Field)
		if err != nil {
			return err
		}

		filter.Field = tr

		return nil
	})

	if err != nil {
		return err
	}

	p.Model = model

	return nil
}

func (p *Params) Translated() bool {
	return p.Model != nil
}
