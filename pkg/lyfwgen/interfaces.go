package lyfwgen

import "go/types"

type Describer interface {
	Describe(obj types.Object, comment, targetDir string) error
}
