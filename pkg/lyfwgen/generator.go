package lyfwgen

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"go/types"
	"path"
	"regexp"
	"strings"

	"github.com/fatih/structtag"
	"golang.org/x/tools/go/packages"
)

type Generator struct {
	fileRx    *regexp.Regexp
	comments  map[*ast.Ident]string
	describer Describer
}

func (g *Generator) extractComments(ppkg *packages.Package) {
	for _, af := range ppkg.Syntax {
		for _, decl := range af.Decls {
			gdecl, ok := decl.(*ast.GenDecl)
			if !ok {
				continue
			}

			var id *ast.Ident

			for _, spec := range gdecl.Specs {
				tspec, ok := spec.(*ast.TypeSpec)
				if !ok {
					continue
				}

				id = tspec.Name
			}

			if id == nil {
				continue
			}

			if gdecl.Doc != nil {
				for _, comment := range gdecl.Doc.List {
					g.comments[id] += comment.Text
				}
			}
		}
	}
}

func (g *Generator) Generate(dir string) error {
	cfg := &packages.Config{
		Dir: dir,
		Mode: packages.NeedName |
			packages.NeedFiles |
			packages.NeedCompiledGoFiles |
			packages.NeedImports |
			packages.NeedTypes |
			packages.NeedTypesSizes |
			packages.NeedSyntax |
			packages.NeedTypesInfo,
		Tests: false,
		ParseFile: func(fset *token.FileSet, filename string, src []byte) (*ast.File, error) {
			base := path.Base(filename)
			if m := g.fileRx.MatchString(base); m {
				return nil, nil
			}

			const mode = parser.AllErrors | parser.ParseComments
			return parser.ParseFile(fset, filename, src, mode)
		},
	}

	pkgs, err := packages.Load(cfg, ".")
	if err != nil {
		return err
	}

	h := TypesHelper{}

	for _, ppkg := range pkgs {
		g.extractComments(ppkg)

		for id, def := range ppkg.TypesInfo.Defs {
			if def == nil || def.Parent() == nil {
				continue
			}

			hasFields := false

			h.IterStructFields(def.Type(), func(field *types.Var, _ *structtag.Tags) {
				if !hasFields {
					hasFields = true
				}
			})

			comment := g.comments[id]
			if !strings.Contains(comment, LyfwIdentifier) {
				continue
			}

			if err := g.describer.Describe(def, comment, dir); err != nil {
				return err
			}
		}
	}

	return nil
}

func NewGenerator(describer Describer) *Generator {
	return &Generator{
		fileRx:    regexp.MustCompile(fmt.Sprintf(`.+?_%s.go`, LyfwIdentifier)),
		comments:  make(map[*ast.Ident]string),
		describer: describer,
	}
}
