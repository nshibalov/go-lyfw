package lyfwgen

import (
	"bytes"
	"fmt"
	"go/format"
	"go/types"
	"io/ioutil"
	"log"
	"path"
	"strings"
	"text/template"
)

const (
	DescriberTemplatesDir = "template"

	DescriberFuncHasMethod = "hasMethod"
	DescriberFuncStoreName = "storeName"
)

type describer struct {
	name string
	obj  types.Object
	t    *template.Template
	h    TypesHelper
	buf  bytes.Buffer
}

func (d *describer) format() []byte {
	src, err := format.Source(d.buf.Bytes())
	if err != nil {
		// Should never happen, but can arise when developing this code.
		// The user can compile the output to see the error.
		log.Printf("warning: internal error: invalid Go generated: %s", err)
		log.Printf("warning: compile the package to analyze the error")

		return d.buf.Bytes()
	}

	return src
}

func (d *describer) hasMethod(name string) bool {
	return d.h.HasMethod(d.obj, name)
}

func (d *describer) Describe(obj types.Object, comment, targetDir string) error {
	d.buf.Reset()
	d.obj = obj

	if err := d.t.Execute(&d.buf, struct {
		Obj     types.Object
		Comment string
	}{Obj: d.obj, Comment: comment}); err != nil {
		return err
	}

	return ioutil.WriteFile(
		path.Join(targetDir, fmt.Sprintf("%s_%s_%s.go", strings.ToLower(d.obj.Name()), d.name, LyfwIdentifier)),
		d.format(),
		0600)
}

func NewDescriber(name, templateName, templatesDirPathPrefix string, funcMap template.FuncMap) (Describer, error) {
	d := &describer{
		name: name,
	}

	defaultFuncMap := template.FuncMap{
		DescriberFuncHasMethod: d.hasMethod,
	}

	for k, v := range funcMap {
		defaultFuncMap[k] = v
	}

	t, err := template.New("").Funcs(defaultFuncMap).ParseFiles(path.Join(templatesDirPathPrefix, DescriberTemplatesDir, templateName))
	if err != nil {
		return nil, err
	}

	d.t = t.Lookup(templateName)

	return d, nil
}
