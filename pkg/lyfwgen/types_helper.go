package lyfwgen

import (
	"fmt"
	"go/types"

	"github.com/fatih/structtag"
)

type TypesHelper struct{}

func (h TypesHelper) HasMethod(obj types.Object, name string) bool {
	found, _, _ := types.LookupFieldOrMethod(obj.Type(), true, obj.Pkg(), name)
	if _, ok := found.(*types.Func); ok {
		return true
	}

	return false
}

func (h TypesHelper) IterStructFields(typ types.Type, callback func(field *types.Var, tags *structtag.Tags)) {
	switch v := typ.(type) {
	case *types.Struct:
		for i := 0; i < v.NumFields(); i++ {
			f := v.Field(i)

			if f.Embedded() {
				h.IterStructFields(f.Type(), callback)
			} else {
				tags, err := structtag.Parse(v.Tag(i))
				if err != nil {
					return
				}

				callback(f, tags)
			}
		}
	case *types.Named:
		h.IterStructFields(v.Underlying(), callback)
	}
}

func (h TypesHelper) IterStructMethods(typ types.Type, callback func(method *types.Func)) {
	switch v := typ.(type) {
	case *types.Struct:
		for i := 0; i < v.NumFields(); i++ {
			f := v.Field(i)

			if f.Embedded() {
				h.IterStructMethods(f.Type(), callback)
			}
		}
	case *types.Named:
		for i := 0; i < v.NumMethods(); i++ {
			callback(v.Method(i))
		}
	}
}

func (h TypesHelper) GetTypePkgID(t types.Type) string {
	switch v := t.(type) {
	case *types.Named:
		return v.Obj().Pkg().Path()
	case *types.Pointer:
		return h.GetTypePkgID(v.Elem())
	case *types.Slice:
		return h.GetTypePkgID(v.Elem())
	}

	return ""
}

func (h TypesHelper) GetTypeName(t types.Type, targetPkg *types.Package) string {
	switch v := t.(type) {
	case *types.Named:
		if targetPkg == nil {
			return v.Obj().Name()
		}

		nPkg := v.Obj().Pkg()
		if nPkg.Path() != targetPkg.Path() {
			return fmt.Sprintf("%s.%s", nPkg.Name(), v.Obj().Name())
		}

		return v.Obj().Name()
	case *types.Basic:
		return v.Name()
	case *types.Pointer:
		return h.GetTypeName(v.Elem(), targetPkg)
	case *types.Slice:
		return h.GetTypeName(v.Elem(), targetPkg)
	}

	return ""
}

func (h TypesHelper) GetRawTypeName(t types.Type, targetPkg *types.Package) string {
	switch v := t.(type) {
	case *types.Named:
		if targetPkg == nil {
			return v.Obj().Name()
		}

		nPkg := v.Obj().Pkg()
		if nPkg.Path() != targetPkg.Path() {
			return fmt.Sprintf("%s.%s", nPkg.Name(), v.Obj().Name())
		}

		return v.Obj().Name()
	case *types.Basic:
		return v.Name()
	case *types.Pointer:
		return fmt.Sprintf("*%s", h.GetTypeName(v.Elem(), targetPkg))
	case *types.Slice:
		return fmt.Sprintf("[]%s", h.GetTypeName(v.Elem(), targetPkg))
	}

	return ""
}
