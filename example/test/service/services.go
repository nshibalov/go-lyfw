package service

import "gitlab.com/nshibalov/go-lyfw/example/test/model"

// lyfw
type Service struct{}

func (s *Service) SomeFunc() {}

func (s *Service) Sum(a, b int) int {
	return a + b
}

func (s *Service) CreateCity(name string) *model.City {
	return &model.City{Name: name}
}
