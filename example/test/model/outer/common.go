package outer

import "time"

type Common struct {
	ID        int        `json:"id" lyfw_id:""`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `json:"deletedAt"`
}
