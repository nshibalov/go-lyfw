package model

import (
	"gitlab.com/nshibalov/go-lyfw/example/test/model/outer"
)

// lyfw_storeName: orders
type Order struct {
	ID   string `json:"id" lyfw_id:""`
	Name string `json:"name"`

	UserID int   `json:"userId"`
	User   *User `json:"user" lyfw_rel:"hasOne,UserID"`
}

// lyfw_storeName: bads
type Bad struct {
	ID   int    `lyfw_id:""`
	Name string `json:"name"`
}

// lyfw_storeName: users
type User struct {
	outer.Common

	Name string `json:"name"`

	Bad *Bad `json:"bad" lyfw_rel:"hasOne"`

	OrderID int    `json:"orderId"`
	Order   *Order `json:"order" lyfw_rel:"hasOne"`

	Addresses []Address `json:"addresses" lyfw_rel:"hasMany,UserID"`
}

// lyfw_storeName: addresses
type Address struct {
	outer.Common

	AddressLine1 string `json:"addressLine1"`

	UserID int   `json:"userId"`
	User   *User `json:"user" lyfw_rel:"hasOne,UserID"`
}

// lyfw_externalTag: yaml
type City struct {
	outer.Common

	Name string `yaml:"name" db:"title"`

	Users []User `yaml:"users" lyfw_rel:"manyToMany,city_to_users,city_id,user_id"`
}

func (t *City) StoreName() string { return "cities" }
