module gitlab.com/nshibalov/go-lyfw

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fatih/structtag v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/magiconair/properties v1.8.4
	github.com/stretchr/testify v1.6.1
	golang.org/x/tools v0.0.0-20201116182000-1d699438d2cf
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
