package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwgen"
	"gitlab.com/nshibalov/go-lyfw/pkg/lyfwstore"
)

const ErrCode = 1

func myUsage() {
	fmt.Println("Usage: gen [flags] [directory]")
	flag.PrintDefaults()
}

func main() {
	flag.Usage = myUsage

	flag.Parse()

	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(ErrCode)
	}

	dir := flag.Arg(0)

	d, err := lyfwstore.NewDescriber("../..")
	if err != nil {
		log.Fatal(err)
	}

	g := lyfwgen.NewGenerator(d)
	if err := g.Generate(dir); err != nil {
		log.Fatal(err)
	}
}
